<?php

/**
 * @file
 * Babeltheque module.
 */

/**
 * Implements hook_menu().
 */
function babeltheque_menu() {
  $items['admin/config/services/babeltheque'] = array(
    'title' => 'Babeltheque',
    'description' => 'Configure Babeltheque module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('babeltheque_html_form'),
    'access arguments' => array('access administration pages'),
    'file' => 'babeltheque.admin.inc',
  );
  $items['admin/config/services/babeltheque/html'] = array(
    'title' => 'HTML settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/services/babeltheque/content_types'] = array(
    'title' => 'Content types settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('babeltheque_types_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'babeltheque.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/services/babeltheque/content_types/add'] = array(
    'title' => 'New Babeltheque rule',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('babeltheque_services_add_form'),
    'file' => 'babeltheque.admin.inc',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/services/babeltheque/content_types/edit/%/%'] = array(
    'title' => 'New Babeltheque rule',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('babeltheque_services_edit_form', 6, 7),
    'file' => 'babeltheque.admin.inc',
    'access arguments' => array('administer site configuration'),
  );
  $items['admin/config/services/babeltheque/export_ean'] = array(
    'title' => 'Export EAN/ISBN',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('babeltheque_export_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'babeltheque.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  return $items;
}

/**
 * Implements hook_node_view().
 */
function babeltheque_node_view($node, $view_mode, $langcode) {
  module_load_include('inc', 'babeltheque', "includes/babeltheque.db");

  // Retrieves rules and configuration set
  // in Admin > Configuration > Babeltheque.
  $conf = babeltheque_get_services();

  // Retrieves availables services.
  $services_desc = babeltheque_services();

  // We add babeltheque section only if
  // node type and view mode match
  // a defined rule.
  if (isset($conf[$node->type][$view_mode])) {
    drupal_add_js(variable_get('babeltheque_js_link', 'http://www.babeltheque.com/bw_XX.js'), array('type' => 'external', 'scope' => 'footer'));
    $isbn_field = $conf[$node->type][$view_mode]['node_field_isbn'];

    if (isset($node->{$isbn_field}['und'][0]['value'])) {
      // Get EAN formatted number.
      $ean = isbn2ean($node->{$isbn_field}['und'][0]['value']);

      // Services to add to the page is a
      // comma separated list.
      $services = explode(',', $conf[$node->type][$view_mode]['services']);
      if ($ean) {
        // Add babeltheque expected html code to the page
        // for each configured service.
        foreach ($services as $service) {
          $field_name = $services_desc[$service]['field'];
          $markup = $services_desc[$service]['block'];
          $variable = 'babeltheque_' . $service;
          $default_html = '<div id="' . $services_desc[$service]['css_id'] . '"></div>';
          $markup = variable_get($service, $default_html);

          // Add empty fields to the node.
          // The field will be rendered or not
          // depending on content display configuration.
          $node->content[$field_name] = array(
            '#markup' => $markup,
          );
        }

        // Add a field to the node containing
        // ISBN/EAN of the node. It will be used
        // by babeltheque to match a coresponding biblio.
        $node->content['field_babeltheque_numisbn'] = array(
          '#markup' => "<input type='hidden' id='BW_id_isbn' value=$ean\>",
        );
      }
    }
  }
}

/**
 * Implements hook_ds_fields_info().
 */
function babeltheque_ds_fields_info($entity_type) {
  $fields = array();

  // Retrieves availables services.
  $services_desc = babeltheque_services();

  // Each service is added to a ds field.
  foreach ($services_desc as $service => $info) {
    $fields["field_babeltheque_$service"] = array(
      'title' => t('Babelthèque @service', array('@service' => $service)),
      'field_type' => DS_FIELD_TYPE_IGNORE,
      'ui_limit' => array('*|*'),
    );
  }

  // Add numisbn which is not a service
  // but is required for babeltheque.
  $fields['field_babeltheque_numisbn'] = array(
    'title' => t('Babelthèque ISBN (hidden)'),
    'field_type' => DS_FIELD_TYPE_IGNORE,
    'ui_limit' => array('*|*'),
  );

  return array('node' => $fields);
}

/**
 * Return availables services for babeltheque.
 *
 * @return array
 *   Services array keyed by name.
 */
function babeltheque_services() {
  $services = array(
    'rates' => array(
      'field' => 'field_babeltheque_rates',
      'title' => t('Average rating'),
      'css_id' => 'BW_notes',
      'block' => variable_get('babeltheque_rates', '<div id="BW_notes"></div>'),
    ),
    'critics' => array(
      'field' => 'field_babeltheque_critics',
      'title' => t('Critics'),
      'css_id' => 'BW_critiques',
      'block' => variable_get('babeltheque_critics', '<div id="BW_critiques"></div>'),
    ),
    'critics_opac' => array(
      'field' => 'field_babeltheque_critics_opac',
      'title' => t('Critics OPAC'),
      'css_id' => 'BW_critiques_OPAC',
      'block' => variable_get('babeltheque_critics_opac', '<div id="BW_critiques_OPAC"></div>'),
    ),
    'critics_pro' => array(
      'field' => 'field_babeltheque_critics_pro',
      'title' => t('Critics pro'),
      'css_id' => 'BW_critiques_pro',
      'block' => variable_get('babeltheque_critics_pro', '<div id="BW_critiques_pro"></div>'),
    ),
    'quotes' => array(
      'field' => 'field_babeltheque_quotes',
      'title' => t('Quotes'),
      'css_id' => 'BW_citations',
      'block' => variable_get('babeltheque_quotes', '<div id="BW_citations"></div>'),
    ),
    'tags' => array(
      'field' => 'field_babeltheque_tags',
      'title' => t('Tags'),
      'css_id' => 'BW_etiquettes',
      'block' => variable_get('babeltheque_tags', '<div id="BW_etiquettes"></div>'),
    ),
    'suggestions' => array(
      'field' => 'field_babeltheque_suggestions',
      'title' => t('Suggestions'),
      'css_id' => 'BW_suggestions',
      'block' => variable_get('babeltheque_suggestions', '<div id="BW_suggestions"></div>'),
    ),
    'videos' => array(
      'field' => 'field_babeltheque_videos',
      'title' => t('Videos'),
      'css_id' => 'BW_videos',
      'block' => variable_get('babeltheque_videos', '<div id="BW_videos"></div>'),
    ),
    'podcasts' => array(
      'field' => 'field_babeltheque_podcasts',
      'title' => t('Podcasts'),
      'css_id' => 'BW_podcasts',
      'block' => variable_get('babeltheque_podcasts', '<div id="BW_podcasts"></div>'),
    ),
  );

  return $services;
}

/**
 * Transform ISBN to EAN.
 *
 * Babeltheque needs EAN to match coresponding records.
 * 
 * @param string $isbn
 *   ISBN to strnsform.
 *
 * @return string
 *   EAN.
 */
function isbn2ean($isbn) {
  // Remove spaces and dashes from ISBN.
  $isbn = str_replace("-", "", $isbn);
  $isbn = str_replace(" ", "", $isbn);

  // ISBN may be 9 characters.
  if (strlen($isbn) < 10) {
    $isbn = $isbn . "X";
  }

  // ISBN 10 is ready to become an ISBN 13.
  if (strlen($isbn) == 10) {
    $isbn = substr($isbn, 0, -1);
    $isbn = "978" . $isbn;
    $code = $isbn;
    $isbn = str_split($isbn);
    $i = 0;
    $i2 = 0;
    $r = '';
    while ($i2 <= 11) {
      if ($i2 % 2 == 0) {
        $p = "1";
      }
      else {
        $p = "3";
      }
      $r += $isbn[$i] * $p;
      if ($isbn[$i] != "-") {
        $i2++;
      }
      $i++;
    }
    $q = floor($r / 10);
    $isbn = 10 - ($r - $q * 10);
    if ($isbn == "10") {
      $isbn = "0";
    }
    $isbn = $code . $isbn;
  }
  return $isbn;
}
