<?php
/**
 * @file
 * This file contains setting forms for babeltheque module.
 */

/**
 * Form constructor for the babeltheque global settings form.
 *
 * This form allows to configure html section for each service
 * and add custom babeltheque js script.
 *
 * @ingroup forms
 */
function babeltheque_html_form($form, $form_state) {

  $string = "<div>" . t('Here is the HTML settings for Babeltheque module.All you have to do is to enter informations you can found
    at your') . l(t('Babeltheque admin interface'), 'http://www.babeltheque.com/code_html.php') . ".</div>";
  $form['help'] = array(
    '#markup' => $string,
  );

  $form['html'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML to insert'),
    '#weight' => 4,
  );
  $form['html']['babeltheque_js_link'] = array(
    '#type' => 'textfield',
    '#title' => t('js script to insert'),
    '#default_value' => variable_get('babeltheque_js_link', 'http://www.babeltheque.com/bw_XX.js'),
    '#size' => 60,
    '#required' => TRUE,
  );

  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocks to insert'),
    '#weight' => 5,
  );

  $services = babeltheque_services();

  foreach ($services as $service => $info) {
    $variable = "babeltheque_$service";
    $default_html = '<div id="' . $info['css_id'] . '"></div>';
    $form['blocks']["babeltheque_$service"] = array(
      '#type' => 'textarea',
      '#title' => $info['title'],
      '#cols' => 40,
      '#rows' => 3,
      '#resizable' => FALSE,
      '#default_value' => variable_get($variable, $default_html),
    );
  }

  return system_settings_form($form);
}

/**
 * Form constructor for the babeltheque rules overviewm.
 *
 * @see babeltheque_types_form_submit()
 * @ingroup forms
 */
function babeltheque_types_form($form, $form_state) {
  module_load_include('inc', 'babeltheque', "includes/babeltheque.db");
  $content_types = node_type_get_types();
  $conf = babeltheque_get_services();
  $entity = entity_get_info('node');
  $view_modes = $entity['view modes'];

  $babeltheque_services = babeltheque_services();

  $header = array(
    'type' => t('Content type'),
    'view_mode' => t('View mode'),
    'node_field_isbn' => t('ISBN field'),
    'services' => t('Services'),
    'operations' => t('Operations'),
  );

  $data = array();
  foreach ($conf as $type => $modes) {
    $fields = field_info_instances('node', $type);

    foreach ($modes as $mode => $values) {
      $services = array();
      foreach (explode(',', $values['services']) as $service) {
        $services[] = $babeltheque_services[$service]['title'];
      }

      $data[$type . "___" . $mode]['type'] = $content_types[$type]->name;
      $data[$type . "___" . $mode]['view_mode'] = $view_modes[$mode]['label'];
      $data[$type . "___" . $mode]['node_field_isbn'] = $fields[$values['node_field_isbn']]['label'];
      $data[$type . "___" . $mode]['services'] = implode(', ', $services);
      $data[$type . "___" . $mode]['operations'] = l(t('edit'), "admin/config/services/babeltheque/content_types/edit/$type/$mode");
    }
  }

  $string = "<div>" . t('Here is the content types settings for Babeltheque services. You can select one or more services you want to be displayed in each content type.') . "</div>";
  $string .= "<div>" . t('When checking one or more services for a content type you have to <b>select the fields which will contain the EAN/ISBN value</b>. This value will be transformed if needed.
    For instance, a ISBN 10 will be transformed to an ISBN 13.') . "</div>";
  $form['help'] = array(
    '#markup' => $string,
  );
  $form['babeltheque_rules'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $data,
    '#empty' => t('No rules yet'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('delete'),
    '#weight' => 15,
  );

  return $form;
}

/**
 * Form submitting handler for babeltheque_types_form().
 */
function babeltheque_types_form_submit($form, $form_state) {
  $values = $form_state['values'];
  foreach ($values['babeltheque_rules'] as $key => $ckecked) {
    if ($ckecked) {
      list($type, $mode) = explode('___', $key);
      babeltheque_services_del($type, $mode);
    }
  }
}

/**
 * Form constructor for adding rule.
 *
 * This form allows to add rules (content types, view mode)
 * that will match pages where adding babeltheque services.
 *
 * @see babeltheque_services_add_form_submit()
 * @ingroup forms
 */
function babeltheque_services_add_form($form, $form_state) {
  // Content type options.
  $content_types = node_type_get_types();
  $options_type = array('none' => 'None');
  foreach ($content_types as $type => $content_type) {
    $options_type[$type] = $content_type->name;
  }

  // View modes options.
  $entity = entity_get_info('node');
  $view_modes = array('none' => 'None');
  foreach ($entity['view modes'] as $name => $view_mode) {
    $view_modes[$name] = $view_mode['label'];
  }

  // Fields options.
  $fields_options = array('none' => 'None');

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => $options_type,
    '#default_value' => 'none',
    '#ajax' => array(
      'callback' => 'babeltheque_fields_list_callback',
      'wrapper' => 'replace_fields_choice_div',
    ),
  );
  $form['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View mode'),
    '#options' => $view_modes,
    '#default_value' => 'none',
  );
  $form['node_field_isbn'] = array(
    '#type' => 'select',
    '#title' => t('ISBN Field'),
    '#options' => !empty($form_state['values']['type']) ?
    babeltheque_get_fields_options($form_state['values']['type']) :
    $fields_options,
    '#default_value' => 'none',
    '#description' => t('Select here the field which contains the EAN/ISBN value.<br /> You may need to create one'),
    '#prefix' => '<div id="replace_fields_choice_div">',
    '#suffix' => '</div>',
  );

  $babeltheque_options = array();
  $babeltheque_services = babeltheque_services();
  foreach ($babeltheque_services as $name => $info) {
    $babeltheque_options[$name] = $info['title'];
  }

  $form['services'] = array(
    '#type' => 'checkboxes',
    '#options' => $babeltheque_options,
    '#title' => t('Services'),
    '#description' => t('What services you want to be inserted in this content type.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Get availables fields for a node type.
 *
 * @param sting $node_type
 *   Node type machine name.
 *
 * @return array
 *   Availables fields list keyed by field name.
 */
function babeltheque_get_fields_options($node_type) {
  $fields = field_info_instances('node', $node_type);
  $fields_options = array('none' => 'None');
  foreach ($fields as $name => $field) {
    $fields_options[$name] = $field['label'];
  }
  return $fields_options;
}

/**
 * Form callback.
 */
function babeltheque_fields_list_callback($form, $form_state) {
  return $form['node_field_isbn'];
}

/**
 * Form submitting handler for babeltheque_services_add_form().
 */
function babeltheque_services_add_form_submit($form, $form_state) {
  module_load_include('inc', 'babeltheque', "includes/babeltheque.db");
  $values = $form_state['values'];

  $services = array();
  foreach ($values['services'] as $id => $checked) {
    if ($checked) {
      $services[] = $id;
    }
  }
  $values['services'] = implode(',', $services);

  $conf = babeltheque_get_services();
  if (isset($conf[$values['type']][$values['view_mode']])) {
    babeltheque_services_mod($values);
  }
  else {
    babeltheque_services_add($values);
  }
  drupal_goto('admin/config/services/babeltheque/content_types');
}

/**
 * Form constructor for editing rule.
 *
 * @see babeltheque_services_edit_form_submit()
 * @ingroup forms
 */
function babeltheque_services_edit_form($form, $form_state, $type, $view_mode) {
  module_load_include('inc', 'babeltheque', "includes/babeltheque.db");
  // Content type options.
  $content_types = node_type_get_types();
  $options_type = array($type => $content_types[$type]->name);

  // View modes options.
  $entity = entity_get_info('node');
  $view_modes = array($view_mode => $entity['view modes'][$view_mode]['label']);

  $conf = babeltheque_get_services();
  $default = $conf[$type][$view_mode];

  // Fields options.
  $fields = field_info_instances('node', $type);
  $fields_options = array();
  foreach ($fields as $name => $field) {
    $fields_options[$name] = $field['label'];
  }

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => $options_type,
    '#disabled' => TRUE,
    '#default_value' => $type,
  );
  $form['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View mode'),
    '#options' => $view_modes,
    '#disabled' => TRUE,
    '#default_value' => $view_mode,
  );
  $form['node_field_isbn'] = array(
    '#type' => 'select',
    '#title' => t('ISBN Field'),
    '#options' => $fields_options,
    '#default_value' => $default['node_field_isbn'],
    '#description' => t('Select here the field which contains the EAN/ISBN value.<br /> You may need to create one'),
  );

  $babeltheque_options = array();
  $babeltheque_services = babeltheque_services();
  foreach ($babeltheque_services as $name => $info) {
    $babeltheque_options[$name] = $info['title'];
  }

  $form['services'] = array(
    '#type' => 'checkboxes',
    '#options' => $babeltheque_options,
    '#default_value' => explode(',', $default['services']),
    '#title' => t('Services'),
    '#description' => t('What services you want to be inserted in this content type.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Form submitting handler for babeltheque_services_edit_form().
 */
function babeltheque_services_edit_form_submit($form, $form_state) {
  module_load_include('inc', 'babeltheque', "includes/babeltheque.db");
  $values = $form_state['values'];

  $services = array();
  foreach ($values['services'] as $id => $checked) {
    if ($checked) {
      $services[] = $id;
    }
  }
  $values['services'] = implode(',', $services);

  if (count($services)) {
    babeltheque_services_mod($values);
  }
  else {
    babeltheque_services_del($values['type'], $values['view_mode']);
  }
  drupal_goto('admin/config/services/babeltheque/content_types');
}

/**
 * Form constructor for export ean from nodes.
 *
 * @see babeltheque_export_form_submit()
 * @ingroup forms
 */
function babeltheque_export_form($form, $form_state) {
  $content_types = node_type_get_types();
  foreach ($content_types as $type => $content_type) {
    $fields = field_info_instances('node', $type);
    $fields_options = array('none' => t('None'));
    foreach ($fields as $fieldname => $field) {
      $fields_options[$fieldname] = $field['label'];
    }
    $form[$type] = array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('container-inline')),
    );
    $form[$type][$type . "_yes"] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($content_type->name),
    );
    $form[$type][$type . "_field"] = array(
      '#type' => 'select',
      '#title' => t('EAN Field'),
      '#options' => $fields_options,
      '#default_value' => isset($default['ean_field']) ? $default['ean_field'] : 'none',
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#weight' => 15,
  );

  return $form;
}

/**
 * Form submitting handler for babeltheque_export_form().
 */
function babeltheque_export_form_submit($form, $form_state) {
  $values = $form_state['values'];
  $content_types = node_type_get_types();
  $content = "\"ISBN\"\n";
  foreach ($content_types as $type => $content_type) {
    if (isset($values[$type . '_yes']) && $values[$type . '_field'] != 'none') {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->propertyCondition('type', $type);

      $entities = $query->execute();
      $nodes = node_load_multiple(array_keys($entities['node']));

      foreach ($nodes as $node) {
        if (isset($node->{$values[$type . '_field']}['und'][0]['value'])) {
          $isbn = isbn2ean($node->{$values[$type . '_field']}['und'][0]['value']);
          $content .= "\"" . $isbn . "\"\n";
        }
      }
    }
  }

  $today = date("Ymd_His");
  $filename = "babeltheque_export_$today.csv";
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename=' . $filename);
  header('Content-Transfer-Encoding: binary');
  ob_clean();
  flush();
  print $content;
  exit;
}
