<?php
/**
 * @file
 * This file contains all db functions for babeltheque module.
 */

/**
 * Create a rule.
 *
 * @param array $values
 *   Values to insert.
 */
function babeltheque_services_add($values) {
  db_insert('babeltheque_services')
    ->fields(array(
      'type' => $values['type'],
      'view_mode' => $values['view_mode'],
      'node_field_isbn' => $values['node_field_isbn'],
      'services' => $values['services'],
    ))
    ->execute();
}

/**
 * Change isbn field and services for a given rule.
 *
 * @param array $values
 *   New isbn field and new services for
 *   a rule (node type and view mode).
 */
function babeltheque_services_mod($values) {
  db_update('babeltheque_services')
    ->fields(array(
      'node_field_isbn' => $values['node_field_isbn'],
      'services' => $values['services'],
    ))
  ->condition('type', $values['type'])
  ->condition('view_mode', $values['view_mode'])
  ->execute();
}

/**
 * Delete a rule.
 *
 * @param string $type
 *   Node type.
 *
 * @param string $view_mode
 *   View mode.
 */
function babeltheque_services_del($type, $view_mode) {
  db_delete('babeltheque_services')
    ->condition('type', $type)
    ->condition('view_mode', $view_mode)
    ->execute();
}

/**
 * Return all rules.
 *
 * @return array
 *   All existing rules.
 */
function babeltheque_get_services() {
  $select = db_select('babeltheque_services', 'serv')
    ->fields('serv');

  $conf = array();
  $result = $select->execute();

  while ($row = $result->fetchAssoc()) {
    $conf[$row['type']][$row['view_mode']] = $row;
  }

  return $conf;
}
